#include "i2c.h"

// https://datagor.ru/microcontrollers/microcontrollers-code-library/2684-neskolko-funkciy-dlya-programnoy-realizacii-protokola-i2c-na-avr.html
//  Передачу инициирует ведущее устройство, формируя состояние СТАРТ на шине.
//  С этой функции я и начал.

/*void i2c_start(void)
{
    asm("cli");
    txsda;
    clrsda;
    delay_us(i2c_time);
    txscl;
    clrscl;
    delay_us(i2c_time);
    asm("sei");
};*/
// Функция ничего не возвращает, и на время своего выполнения запрещает все прерывания.

// Окончание передачи также инициирует ведущее устройство формируя состояние СТОП на шине.
/*void i2c_stop(void)
{
    asm("cli");
    txsda;
    clrsda;
    delay_us(i2c_time);
    rxscl;
    delay_us(i2c_time);
    rxsda;
    asm("sei");
};*/

// Передача одного байта данных:

/*char i2c_tx(char data)
{
    asm("cli");
    char x;
    char b = 1;
    for (x = 8; x; x--) // цикл на 8 передаваемых бит
    {
        if ((data & 0x80) == 0) // проверяем старший бит в передаваемом байте
        {
            txsda; // выдаем бит данных на SDA
            clrsda;
        }
        else
        {
            rxsda; // или оставляем линию в покое
        };
        txscl; // выдаем такт в линию SCL
        clrscl;
        delay_us(i2c_time);
        data <<= 1; // сдвигаем передаваемые биты влево
        rxscl;
        delay_us(i2c_time << 1);
        txscl;
        clrscl;
        delay_us(i2c_time);
    };
    rxscl;
    delay_us(i2c_time);
    rxsda;
    if ((i2c_pin & i2c_readstatus) == i2c_readstatus)
    {
        b = 0;
    }; // считываем возможный ACK бит
    delay_us(i2c_time);
    txscl;
    clrscl;
    delay_us(i2c_time << 1);
    asm("sei");
    return b;
};*/
// Функция возвращает 1, если slave подтвердил передачу и 0 если не подтвердил.

// Чтение байта:
// Передаем в качестве параметра ack единицу, если хотим, чтобы наш master дал
// подтверждение и любое другое число, чтобы ответить NACK.
/*char i2c_rx(char ack)
{
    asm("cli");
    char x;                 // счетчик
    char data = 0;          // принимаемый байт
    rxsda;                  // настраиваем sda на чтение
    for (x = 0; x < 8; x++) // цикл приема бит
    {
        txscl; // выдаем такт на SCL
        clrscl;
        delay_us(i2c_time);
        rxscl;
        delay_us(i2c_time);
        if ((i2c_pin & i2c_readstatus) == i2c_readstatus) // считываем из порта
        {
            // setb((7 - x), data); // если считали единицу - устанавливаем соответствующий бит
            data |= (1 << (7 - x)); // если считали единицу - устанавливаем соответствующий бит
        };
        delay_us(i2c_time);
        txscl;
        clrscl;
        delay_us(i2c_time);
    };
    if (ack == 1) //выдаем или не выдаем ACK на шину
    {
        txsda;
        clrsda;
    }
    else
    {
        rxsda;
    };
    txscl;
    clrscl;
    delay_us(i2c_time);
    rxscl;
    delay_us(i2c_time << 1);
    txscl;
    clrscl;
    delay_us(i2c_time);
    asm("sei");
    return data;
};*/

// В качестве примера приведу функцию записи в аудио процессор tda7439.
void loadtoTDA(void)
{
    uint8_t p;
    char param[8] = {"1"};
    I2c iii;
    iii.i2c_start();
    iii.i2c_tx(0x88); // адрес тда
    iii.i2c_tx(0x10); // режим записи в тда - инкрементный
    for (p = 0; p < 8; p++)
    {
        iii.i2c_tx(param[p]); // передаем массив с параметрами для тда
    };
    iii.i2c_stop();
};



