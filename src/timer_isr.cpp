#include "io.h"
#include "timer_isr.h"
#include "mcu.h"
//  #include "inttypes.h"
#include "avr/interrupt.h"
#include "avr/io2313.h"

void timer_ini(void)

{
  //настройка таймеров

  // TCCR1B |= (1<<CTC1); // устанавливаем режим СТС (сброс по совпадению)
  // TIMSK |= (1<<OCIE1A); //устанавливаем бит разрешения прерывания 1ого счетчика по совпадению с OCR1A(H и L)
  
    //TCCR0 &= 0b0000011; //СК/64 делитель тактовой частоты для таймера 0
  MCU_TCCR0 |= _BV(CS00) | _BV(CS01); // clk/64
  //настройка и управление прерываниями

  // Бит 7 - I: Общее разрешение прерываний. Для разрешения прерываний этот бит должен быть
  // установлен в единицу. Управление отдельными прерываниями производится регистром маски
  // прерываний - GIMSK/TIMSK. Если флаг сброшен (0), независимо от состояния GIMSK/TIMSK,
  // прерывания не разрешены. Бит I очищается аппаратно после входа в прерывание и вос-
  // станавливается командой RETI, для разрешения обработки следующих прерываний.

  // SREG |= 1<<SREG_I; //разрешение глобального прерывания sei();
  // SREG &= 0x7F; //запрещение глобального прерывания  cli();
  
  //TIMSK |= 1 << TOIE0; //Разрешение прерывания по переполнению таймера/счетчика 0.
  TIMSK |= _BV(TOIE0);
  
  //настраиваем пин для вывода тестового (для проверик работы таймера)
  TIM0_DDR = 1;  //пин как вывход
  TIM0_PORT = 0; //на пине 0
}

// софт таймер http://we.easyelectronics.ru/Soft/samyy-prostoy-programmnyy-taymer.html

void timer_set(Timer_t *t, time_t interval)
{
  t->interval = interval;
  t->start = get_time();
  t->flag = started;
}

void timer_reset(Timer_t *t)
{
  t->start += t->interval;
  t->flag = started;
}

void timer_restart(Timer_t *t)
{
  t->start = get_time();
  t->flag = started;
}


Timer_flag timer_expired(Timer_t *t)
{
  if (t->flag)
    return t->flag;
  if ((time_t)(get_time() - t->start) >= (time_t)t->interval)
  {
    t->flag = stopped;
    return first;
  }
  return started;
}

uint64_t get_time(void)
{
  return Timer_Cnt;
}