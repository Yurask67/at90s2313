/*

Copyright 2021 Marc Ketel
SPDX-License-Identifier: Apache-2.0

*/

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/wdt.h>

#include <util/delay.h>

#include "mcu.h"
#include "io.h"
#include "i2c.h"
#include "timer_isr.h"

void setup(void)
{
    wdt_enable(WDTO_500MS);

    // All IO is output.
    DATA_DDR = 1;
    SCK_DDR = 1;
    OE_DDR = 1;
    TIM0_DDR = 1;
    LED_DDR = 1;
    TIM0_DDR = 1;  //пин как вывход
    TIM0_PORT = 0; //на пине 0
    // Configure timer0
     //MCU_TCCR0 |= _BV(CS00) | _BV(CS01); // clk/64
    // TIMSK |= _BV(TOIE0);
    timer_ini();

    loadtoTDA();
    // Enable interrupts
    sei();
}

ISR(MCU_TIMER0_OVF_vect)
{
    const uint8_t clkdiv = 64;
    const uint16_t timerHz = 1000;
    TCNT0 = 256 - (F_CPU / clkdiv / timerHz);

    TIM0_PORT ^= TIM0_PIN; //инверсия
    Timer_Cnt++;           //глобальный счетчик 64 разряда
}

void loop(void)
{
    wdt_reset();
}

__attribute__((OS_main)) int main(void)
{
    setup();
    // http://we.easyelectronics.ru/Soft/samyy-prostoy-programmnyy-taymer.html
    Timer_t tims1; //может быть локальной
    Timer_t tims2; //может быть локальной
    timer_set(&tims1, 10 /*кол-во тиков*/);//100 гц
    timer_set(&tims2, 1000 /*кол-во тиков*/);//1 гц

    for (;;)
    {
        loop();
        if (timer_expired(&tims1))
        {
            timer_reset(&tims1);   //повторить срабатывание с прежним интервалом
                                   //без учета задержки при обработке
            //TIM0_PORT ^= TIM0_PIN; //для проверки;
        }
        if (timer_expired(&tims2))
        {
            timer_reset(&tims2);   //повторить срабатывание с прежним интервалом
                                   //без учета задержки при обработке
           // LED_PORT ^= LED_PIN; //для проверки;
        }
    }
}
