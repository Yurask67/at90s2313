//#define RETI 0b10000000
#define TIM0_PIN PIND4
#define TIM0_PORT REGISTER_BIT(PORTD, 4)
#define TIM0_DDR REGISTER_BIT(DDRD, 4)

#define LED_PIN PIND5
#define LED_PORT REGISTER_BIT(PORTD, 5)
#define LED_DDR REGISTER_BIT(DDRD, 5)



static uint64_t Timer_Cnt=0; //глобальный счетчик прерываний таймера

//для софт таймера http://we.easyelectronics.ru/Soft/samyy-prostoy-programmnyy-taymer.html
typedef unsigned short time_t;

typedef enum
{
  started = 0,
  stopped,
  first
}Timer_flag;

typedef struct{
  time_t start;
  time_t interval;
  Timer_flag flag;
}Timer_t;

void timer_set(Timer_t *t, time_t interval);
void timer_reset(Timer_t *t);
void timer_restart(Timer_t *t);
Timer_flag timer_expired(Timer_t *t);
uint64_t get_time(void);

void timer_ini(void);
